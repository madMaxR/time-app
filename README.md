# Time Application

Time application consists of the frontend and backend parts
Frontend is written with help of the Vue.js framework
Backend is written using Node.js and Express
Database is MySQL

![WebApp](outlines/1.png)

## Quickstart

- Локальное использование:
```shell
docker-compose up
```
- Запуск сервисов с использованием моих публичных образов:
```shell
docker-compose -f docker-compose.pub.yml up
```
